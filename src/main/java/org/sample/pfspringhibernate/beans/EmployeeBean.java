package org.sample.pfspringhibernate.beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.sample.pfspringhibernate.data.Employee;
import org.sample.pfspringhibernate.service.EmployeeService;

@ManagedBean
@ViewScoped
public class EmployeeBean implements Serializable {
	private static final long serialVersionUID = 328293839L;

	@ManagedProperty("#{employeeService}")
	private EmployeeService employeeService;

	private Employee employee;
	private List<Employee> employeeList;

	public EmployeeService getEmployeeService() {
		return employeeService;
	}

	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    protected void listAllEmployees() {
    	employeeList = employeeService.findAll();
    }

    @PostConstruct
    public void init() {
    	listAllEmployees();
        employee = new Employee();
    }

    public void save() {
    	employee = employeeService.save(employee);
        showSuccessSaveMessage();        
        postSave();
    }

    public FacesContext getFacesContext() {
        return FacesContext.getCurrentInstance();
    }    
    
    public void info(String message) {
        getFacesContext().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, null));
    }    

    protected void postSave() {
    	System.out.println(employee.getEmployeeId());
    	employee = employeeService.findById(Employee.class, employee.getEmployeeId());
        listAllEmployees();
    }

    protected void showSuccessSaveMessage() {
        info("Record successfully saved!");
    }

    protected void reloadEmployeeList() {
    	listAllEmployees();
    }

    public void createNewEmployee() {        
        employee = new Employee();
    }

    public void editEmployee(Employee emp) {
        employee = emp;
    }
    
    public void deleteEmployee(Employee emp) {        
    	employeeService.delete(emp);
        showSuccessDeleteMessage();
        postDelete();        
    }

    protected void showSuccessDeleteMessage() {
        info("Employee successfully deleted!");
    }

    protected void postDelete() {
        listAllEmployees();
    }
}
