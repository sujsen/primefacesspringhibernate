package org.sample.pfspringhibernate.service;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import javax.persistence.PersistenceContext;

import org.sample.pfspringhibernate.data.Employee;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class EmployeeService {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional(readOnly = false)
	public void delete(Employee emp){
		entityManager.remove(emp);
		entityManager.flush();		
	}
	public Employee find(Employee emp){
		return findByCriteria(emp);
	}

	public Employee findByNamedQuery(Employee emp) {
		Employee result = entityManager.createNamedQuery("Employee.findByName", Employee.class)
							.setParameter("employeeName", emp.getEmployeeName())
							.getSingleResult();
		if (result != null)
			return result;
		else
			return null;
	}

	public Employee findByCriteria(Employee emp) {	
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Employee> criteriaQuery = cb.createQuery(Employee.class);
		Root<Employee> rootEmployee = criteriaQuery.from(Employee.class);
		ParameterExpression<Long> pExpr = cb.parameter(Long.class);
		criteriaQuery.select(rootEmployee).where(cb.equal(rootEmployee.get("employeeId"), pExpr));
		
		TypedQuery<Employee> query = entityManager.createQuery(criteriaQuery);
		query.setParameter(pExpr, emp.getEmployeeId());
		
		List<Employee> results = query.getResultList();
		if (results != null && results.size() > 0)
			return results.get(0);
		else
			return null;
	}
	public List<Employee> findAll() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Employee> criteriaQuery = cb.createQuery(Employee.class);
		Root<Employee> rootEmployee = criteriaQuery.from(Employee.class);
		CriteriaQuery<Employee> all = criteriaQuery.select(rootEmployee);
		
		TypedQuery<Employee> queryAll = entityManager.createQuery(all);
        return queryAll.getResultList();
	}
	@Transactional(readOnly = false)
	public Employee save(Employee emp) {
		Long empId = emp.getEmployeeId();
		if (empId == null || empId == 0L ) {
			entityManager.persist(emp);	
		}
		else {
			entityManager.merge(emp);
		}
		entityManager.flush();
		System.out.println(emp.getEmployeeId());
		return emp;
	}
	public Employee findById(Class clazz, Object id) {
		Employee found = (Employee) entityManager.find(clazz, id);
		return found;
	}
}
