package org.sample.pfspringhibernate.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@NamedQueries({
	@NamedQuery(name="Employee.findByName",
			query="SELECT emp FROM Employee emp WHERE emp.employeeName = :employeeName"),
	@NamedQuery(name="Employee.findById",
			query="SELECT emp FROM Employee emp WHERE emp.employeeId = :employeeId")
})
@NamedNativeQueries({
	@NamedNativeQuery(name="FindEmployeeByName",
						query="SELECT emp_id, emp_name, emp_hire_date, emp_salary FROM employee WHERE emp_name = ?",
						resultClass=Employee.class)
})
@Entity
@Table(name="employee")
public class Employee implements Serializable {
    private static final long serialVersionUID = 12838399L;

	@SequenceGenerator(name="Emp_Gen", sequenceName="Emp_Seq")
	@Id
	@GeneratedValue(generator="Emp_Gen")
	@Column(name="emp_id")
	private Long employeeId;

	@Column(name="emp_name")
	private String employeeName;

	@Column(name="emp_hire_date")
	@Temporal(TemporalType.TIMESTAMP)
	private Date employeeHireDate;
	
	@Column(name="emp_salary")
	private Double employeeSalary;
	
	public Long getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Long employeeId) {
		this.employeeId = employeeId;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Date getEmployeeHireDate() {
		return employeeHireDate;
	}

	public void setEmployeeHireDate(Date employeeHireDate) {
		this.employeeHireDate = employeeHireDate;
	}

	public Double getEmployeeSalary() {
		return employeeSalary;
	}

	public void setEmployeeSalary(Double employeeSalary) {
		this.employeeSalary = employeeSalary;
	}
}
